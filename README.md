# EventsApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.1.0.

## Prerequisites
- Download and install `Node.js` https://nodejs.org/en/
- In a terminal of your choice, run:
  - `npm install -g @angular/cli` to install the Angular CLI globally
  - `npm install` to install the project dependencies locally

- From inside the root folder of the project, run `ng serve` to run the app. Navigate to `http://localhost:4200/` to see it.

