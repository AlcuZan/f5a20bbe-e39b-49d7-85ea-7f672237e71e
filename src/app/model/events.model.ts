type Pick = {
  id: string
  blurb: string
}

type Venue = {
  id: string
  name: string
  contentUrl: string
  live: boolean
  direction?: string
}

type Artist = {
  id: string
  name: string
}

export interface MusicEvent {
  _id: string
  title: string
  flyerFront: string
  attending: number
  date: string
  dateConverted: string
  startTime?: string
  startDate?: string // Not in JSON
  endTime?: string
  endDate?: string // Not in JSON
  contentUrl: string
  venue: Venue
  pick: Pick
  artists: Artist[]
  city: string
  country: string
  private: boolean
}
