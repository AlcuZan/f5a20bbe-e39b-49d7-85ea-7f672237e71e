export enum Errors {
  WRONG_PASSWORD = "Wrong Password",
  EMAIL_EXISTS = "E-Mail already exists",
  EMAIL_NOT_FOUND = "E-Mail not found"
}
