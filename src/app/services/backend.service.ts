import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, delay, map, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  readonly backendUrl = "https://tlv-events-app.herokuapp.com/events/uk/london"

  constructor(private httpClient: HttpClient) { }

  fetchData<T extends object>(): Observable<T> {
    return this.httpClient.get(this.backendUrl).pipe(
      delay(1200),
      map((json) => json as T),
      catchError((error) => {
        console.log(`Could not fetch ressource from ${this.backendUrl}`)
        throw error
      })
    )
  }
}
