import { homeReducer, HomeState } from '../modules/home/store/home.reducer';
import {
  shoppingCartReducer,
  ShoppingCartState,
} from '../modules/shopping-cart/store/shopping-cart.reducer';
import { authReducer, AuthState } from '../modules/auth/store/auth.reducer';

export interface AppState {
  home: HomeState;
  shoppingCart: ShoppingCartState;
  auth: AuthState;
}

export const reducers = {
  home: homeReducer,
  shoppingCart: shoppingCartReducer,
  auth: authReducer,
};
