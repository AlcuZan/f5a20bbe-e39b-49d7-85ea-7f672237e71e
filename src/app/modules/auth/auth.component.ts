import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '../../store/app.reducer';
import { Store } from '@ngrx/store';
import {
  loginStart,
  resetError,
  signupStart,
  User,
} from './store/auth.actions';
import {
  selectError,
  selectErrorState,
  selectSignedUp,
} from './store/auth.selectors';
import { Observable } from 'rxjs';
import { Errors } from '../../model/error.model';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  isLoginMode: boolean = false;
  form: FormGroup = new FormGroup({});
  error$: Observable<Errors | undefined>;
  errorState$: Observable<boolean>;
  signedUp$: Observable<boolean>;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<AppState>
  ) {
    this.error$ = this.store.select(selectError);
    this.errorState$ = this.store.select(selectErrorState);
    this.signedUp$ = this.store.select(selectSignedUp);
  }

  ngOnInit(): void {
    this.initForm();
    this.signedUp$.subscribe((value) => {
      if (value) {
        this.isLoginMode = true;
      }
    });
  }

  initForm() {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(6)]],
      name: [null, this.isLoginMode ? null : Validators.required],
    });
  }

  getContentClass() {
    return this.isLoginMode ? 'content' : 'content-long';
  }

  getErrorMail(form: FormGroup) {
    if (form.controls['email'].hasError('required')) {
      return 'You must enter a value';
    }
    return form.controls['email'].hasError('email') ? 'Not a valid email' : '';
  }

  getErrorName(form: FormGroup) {
    if (form.controls['name'].hasError('required')) {
      return 'You must enter a value';
    }
    return;
  }

  getErrorPassword(form: FormGroup) {
    if (form.controls['password'].hasError('required')) {
      return 'You must enter a value';
    }
    if (form.controls['password'].errors?.['minlength']) {
      return 'You must enter at least 6 characters';
    }
    return;
  }

  switchLoginMode(form: FormGroup) {
    this.isLoginMode = !this.isLoginMode;
    this.store.dispatch(resetError());
    this.resetForm(form);
  }

  resetForm(form: FormGroup) {
    form.reset();
    this.initForm();
  }

  onSubmit(form: FormGroup) {
    const email = form.value.email;
    const password = form.value.password;
    const name = form.value.name;
    const user: User = { email, password, name };
    if (this.isLoginMode) {
      this.store.dispatch(loginStart({ user }));
    } else {
      this.store.dispatch(signupStart({ user }));
    }
    this.resetForm(form);
  }
}
