import { createAction, props } from '@ngrx/store';
import { Errors } from '../../../model/error.model';

export type User = {
  email: string;
  password: string;
  name?: string;
};

export const loginStart = createAction(
  '[Auth] Login start',
  props<{ user: User }>()
);
export const loginSuccess = createAction(
  '[Auth] login success',
  props<{ user: User }>()
);
export const loginOrSignupFail = createAction(
  '[Auth] login or signup fail',
  props<{ error: Errors }>()
);
export const signupStart = createAction(
  '[Auth] Signup start',
  props<{ user: User }>()
);
export const signupSuccess = createAction('[Auth] signup success');
export const resetError = createAction('[Auth] reset error');
