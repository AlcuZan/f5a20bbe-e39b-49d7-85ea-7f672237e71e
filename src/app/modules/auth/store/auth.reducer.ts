import { createReducer, on } from '@ngrx/store';
import {
  loginOrSignupFail,
  loginSuccess,
  resetError,
  signupSuccess,
  User,
} from './auth.actions';
import { Errors } from '../../../model/error.model';

export const authFeatureKey = 'auth';

export interface AuthState {
  user?: User;
  error?: Errors;
  errorState: boolean;
  signedUp: boolean;
}

export const initialState: AuthState = {
  user: undefined,
  error: undefined,
  errorState: false,
  signedUp: false,
};

export const authReducer = createReducer(
  initialState,
  on(loginSuccess, (state, action) => ({
    ...state,
    user: action.user,
    errorState: false,
  })),
  on(signupSuccess, (state) => ({
    ...state,
    signedUp: true,
    errorState: false,
  })),
  on(loginOrSignupFail, (state, action) => ({
    ...state,
    error: action.error,
    errorState: true,
  })),
  on(resetError, (state) => ({
    ...state,
    errorState: false,
    error: undefined,
  }))
);
