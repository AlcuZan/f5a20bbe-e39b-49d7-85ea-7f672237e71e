import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromAuth from './auth.reducer';
import { AuthState } from './auth.reducer';

export const selectAuthState = createFeatureSelector<AuthState>(
  fromAuth.authFeatureKey
);
export const selectUser = createSelector(
  selectAuthState, //
  (state: fromAuth.AuthState) => state.user
);

export const selectError = createSelector(
  selectAuthState, //
  (state: fromAuth.AuthState) => state.error
);

export const selectErrorState = createSelector(
  selectAuthState, //
  (state: fromAuth.AuthState) => state.errorState
);

export const selectSignedUp = createSelector(
  selectAuthState, //
  (state: fromAuth.AuthState) => state.signedUp
);
