import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { switchMap } from 'rxjs/operators';
import {
  loginOrSignupFail,
  loginStart,
  loginSuccess,
  signupStart,
  signupSuccess,
  User,
} from './auth.actions';
import { AuthService } from '../auth.service';
import { catchError, map, of } from 'rxjs';
import { Router } from '@angular/router';
import { fetchEventData } from '../../home/store/home.actions';

@Injectable()
export class AuthEffects {
  signup$ = createEffect(() =>
    this.actions$.pipe(
      ofType(signupStart),
      switchMap((action) => {
        return this.authService.signup(action.user).pipe(
          map(() => {
            return signupSuccess();
          }),
          catchError((error) => {
            return of(loginOrSignupFail({ error }));
          })
        );
      })
    )
  );

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginStart),
      switchMap((action) => {
        return this.authService.login(action.user).pipe(
          map((user: User) => loginSuccess({ user })),
          catchError((error) => {
            return of(loginOrSignupFail({ error }));
          })
        );
      })
    )
  );

  redirectAfterLogin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginSuccess),
      map(() => {
        this.router.navigate(['home']);
        return fetchEventData();
      })
    )
  );

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router
  ) {}
}
