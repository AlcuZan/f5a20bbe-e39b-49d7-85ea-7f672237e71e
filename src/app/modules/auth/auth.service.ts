import { Injectable } from '@angular/core';
import { of, throwError } from 'rxjs';
import { User } from './store/auth.actions';
import { Errors } from '../../model/error.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor() {}

  signup(user: User) {
    let item: string | null | User = localStorage.getItem(user.email);
    if (item !== null) {
      return throwError(() => Errors.EMAIL_EXISTS);
    }
    localStorage.setItem(
      user.email,
      JSON.stringify({
        user: user.email,
        password: user.password,
        name: user.name,
      })
    );
    return of(item);
  }

  login(user: User) {
    let item: string | null | User = localStorage.getItem(user.email);
    if (item !== null) {
      item = JSON.parse(item) as User;
      if (user.password !== item.password) {
        return throwError(() => Errors.WRONG_PASSWORD);
      }
      return of(item);
    }
    return throwError(() => Errors.EMAIL_NOT_FOUND);
  }
}
