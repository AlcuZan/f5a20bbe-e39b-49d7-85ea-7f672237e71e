import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromShoppingCart from './shopping-cart.reducer';
import {ShoppingCartState} from "./shopping-cart.reducer";

export const selectShoppingCartState = createFeatureSelector<ShoppingCartState>(
  fromShoppingCart.shoppingCartFeatureKey
);

export const selectMusicEventsInCart = createSelector(
  selectShoppingCartState, //
  (state: ShoppingCartState) => state.musicEvents
);

export const selectNumberOfEventsInCart = createSelector(
  selectShoppingCartState, //
  (state: ShoppingCartState) => state.musicEvents.length
);

export const selectDatesOfEventsInShoppingCart = createSelector(
  selectShoppingCartState, //
  (state: ShoppingCartState) => new Set(state.musicEvents.map((ev) => ev.dateConverted))
);
