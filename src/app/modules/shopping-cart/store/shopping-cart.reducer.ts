import {createReducer, on} from '@ngrx/store';
import {MusicEvent} from "../../../model/events.model";
import {addToCart, removeFromCart} from "./shopping-cart.actions";

export const shoppingCartFeatureKey = 'shoppingCart';

export interface ShoppingCartState {
  musicEvents: MusicEvent[]

}

export const initialState: ShoppingCartState = {
  musicEvents: []
};

export const shoppingCartReducer = createReducer(
  initialState,
  on(addToCart, (state, action) => ({
  ...state,
    musicEvents: [...state.musicEvents, action.musicEvent]
  })),
  on(removeFromCart, (state, action) => ({
  ...state,
    musicEvents: state.musicEvents.filter((ev) => ev._id !== action.musicEvent._id)
  })),
);
