import {createAction, props} from '@ngrx/store';
import {MusicEvent} from "../../../model/events.model";

export const addToCart = createAction('[Home] add to cart', props<{ musicEvent: MusicEvent }>());
export const removeFromCart = createAction('[Shopping-cart] remove from cart', props<{musicEvent: MusicEvent}>());
