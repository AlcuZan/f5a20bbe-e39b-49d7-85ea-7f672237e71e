import {Store} from "@ngrx/store";

import { Component, OnInit } from '@angular/core';
import {AppState} from "../../store/app.reducer";
import {selectMusicEventsInCart} from "./store/shopping-cart.selectors";
import {Observable} from "rxjs";
import {MusicEvent} from "../../model/events.model";

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {
  eventData$: Observable<MusicEvent[]>;

  constructor(private store: Store<AppState>) {
    this.eventData$ = this.store.select(selectMusicEventsInCart)
  }

  ngOnInit(): void {
  }

}
