import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducer';
import {
  selectEventData,
  selectSearchResults,
  selectShowLoadingIcon,
} from './store/home.selectors';
import { Observable } from 'rxjs';
import { MusicEvent } from '../../model/events.model';
import { addToCart } from '../shopping-cart/store/shopping-cart.actions';
import { selectNumberOfEventsInCart } from '../shopping-cart/store/shopping-cart.selectors';
import { search } from './store/home.actions';
import { selectUser } from '../auth/store/auth.selectors';
import { User } from '../auth/store/auth.actions';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  eventData$: Observable<MusicEvent[]>;
  eventsInCart$: Observable<number>;
  searchResults$: Observable<MusicEvent[]>;
  eventsInCart: MusicEvent[] = [];
  searchText: string = '';
  searchIsActive: boolean = false;
  showLoadingIcon$: Observable<boolean>;
  user$: Observable<User | undefined>;

  constructor(private store: Store<AppState>) {
    this.eventData$ = this.store.select(selectEventData);
    this.eventsInCart$ = this.store.select(selectNumberOfEventsInCart);
    this.searchResults$ = this.store.select(selectSearchResults);
    this.showLoadingIcon$ = this.store.select(selectShowLoadingIcon);
    this.user$ = this.store.select(selectUser);
  }

  ngOnInit(): void {}

  onClickAddToCart(musicEvent: MusicEvent) {
    this.store.dispatch(addToCart({ musicEvent }));
  }

  onInput(searchText: string) {
    this.searchIsActive = searchText !== '';
    this.store.dispatch(search({ searchText }));
  }
}
