import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromHome from './home.reducer';

export const selectHomeState = createFeatureSelector<fromHome.HomeState>(
  fromHome.homeFeatureKey
);
export const selectEventData = createSelector(
  selectHomeState, //
  (state: fromHome.HomeState) => state.eventData
);
export const selectSearchResults = createSelector(
  selectHomeState, //
  (state: fromHome.HomeState) => state.searchResults
);
export const selectAllDates = createSelector(
  selectHomeState, //
  (state: fromHome.HomeState) => new Set(state.eventData.map((ev) => ev.dateConverted))
);

export const selectShowLoadingIcon = createSelector(
  selectHomeState, //
  (state: fromHome.HomeState) => state.showLoadingIcon
);
