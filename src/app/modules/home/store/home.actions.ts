import {createAction, props} from '@ngrx/store';
import {MusicEvent} from "../../../model/events.model";

export const fetchEventData = createAction('[Home] fetch data');
export const fetchDataSuccess = createAction('[Home] fetch event data success', props<{eventData: MusicEvent[]}>());
export const fetchDataFailure = createAction('[Home] fetch data failure');
export const search = createAction('[Home] search', props<{searchText: string}>());


