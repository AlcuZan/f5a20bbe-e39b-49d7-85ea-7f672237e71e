import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {fetchEventData, fetchDataFailure, fetchDataSuccess} from "./home.actions";
import {catchError, map, of, switchMap} from "rxjs";
import {BackendService} from "../../../services/backend.service";
import {MusicEvent} from "../../../model/events.model";


@Injectable()
export class HomeEffects {

  loadData$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fetchEventData),
        switchMap((action) => {
          return this.backendService.fetchData<MusicEvent[]>().pipe(
            catchError(() => {
              throw "Error fetching data from backend!"
            }),
            map((eventData) => {
              return fetchDataSuccess({eventData})
            }),
            catchError(() => {
              return of(fetchDataFailure())
            })
          )
        })
      ),
  );

  constructor(private actions$: Actions, private backendService: BackendService) {
  }
}
