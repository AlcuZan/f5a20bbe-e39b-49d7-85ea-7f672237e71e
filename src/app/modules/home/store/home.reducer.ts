import {createReducer, on} from '@ngrx/store';
import {MusicEvent} from "../../../model/events.model";
import {fetchDataFailure, fetchDataSuccess, fetchEventData, search} from "./home.actions";
import {addToCart, removeFromCart} from "../../shopping-cart/store/shopping-cart.actions";
import {getConvertedDate, getDate, getDirection, getTime} from "./data-modifications";

export const homeFeatureKey = 'home';
// Placeholder image from https://www.freepik.com/free-photo/excited-audience-watching-confetti-fireworks-having-fun-music-festival-night-copy-space_25566947.htm#query=event&position=0&from_view=search
const placeholderEvent = 'assets/placeholder.jpg'

export interface HomeState {
  eventData: MusicEvent[]
  searchResults: MusicEvent[]
  showLoadingIcon: boolean
  errorState: boolean;
}

export const initialState: HomeState = {
  eventData: [],
  searchResults: [],
  showLoadingIcon: false,
  errorState: false
};

function searchByTitle(searchText: string, musicEvents: MusicEvent[]) {
  const results: MusicEvent [] = []
  for (const musicEvent of musicEvents) {
    if (musicEvent.title.toLowerCase().includes(searchText)) {
      results.push(musicEvent)
    }
  }
  return results
}

function sortEvents(events: MusicEvent[]) {
  return events.sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime())
}

export const homeReducer = createReducer(
  initialState,
  on(fetchDataSuccess, (state, action) => {
    let eventData: MusicEvent[] = []
    action.eventData.forEach((musicEvent) => {
      const newEvent: MusicEvent = {
        ...musicEvent,
        dateConverted: getConvertedDate(musicEvent.date),
        startDate: getDate(musicEvent.startTime),
        startTime: getTime(musicEvent.startTime),
        endDate: getDate(musicEvent.endTime),
        endTime: getTime(musicEvent.endTime),
        flyerFront: musicEvent.flyerFront ?? placeholderEvent,
        venue: {
          ...musicEvent.venue,
          direction: getDirection(musicEvent.city, musicEvent.venue.name)
        }
      }
      eventData.push(newEvent)
    })
    eventData = eventData.filter((ev) => ev.startDate && ev.venue)
    eventData = sortEvents(eventData)
    return {
      ...state,
      eventData,
      showLoadingIcon: false
    }
  }),
  on(fetchDataFailure, (state) => ({
  ...state,
    showLoadingIcon: false,
    errorState: true
  })),
  on(addToCart, (state, action) => ({
    ...state,
    eventData: state.eventData.filter((ev) => ev._id !== action.musicEvent._id)  // delete event from eventData
  })),
  on(removeFromCart, (state, action) => ({
    // Event is removed from cart, so it must be added to the events that are not part of the cart again
  ...state,
    eventData: sortEvents([...state.eventData, action.musicEvent])
  })),
  on(search, (state, action) => {
    const searchResults = searchByTitle(action.searchText, state.eventData)
    return {
    ...state,
      searchResults
    }
  }),
  on(fetchEventData, (state) => ({
  ...state,
    showLoadingIcon: true
  })),
);
