/**
 * Compiles date in the correct format: DD.MM.YYYY.
 */
export function getDate(dateString?: string) {
  if (dateString) {
    const date = new Date(dateString)
    return date.toLocaleDateString()
  }
  return
}

export function convertDay(day: number) {
  switch (day) {
    case 0:
      return "SUN"
    case 1:
      return "MON"
    case 2:
      return "TUE"
    case 3:
      return "WEN"
    case 4:
      return "THU"
    case 5:
      return "FRI"
    case 6:
      return "SAT"
    default:
      return "UNKNOWN"
  }
}

export function convertMonth(day: number) {
  switch (day) {
    case 0:
      return "JAN"
    case 1:
      return "FEB"
    case 2:
      return "MAR"
    case 3:
      return "APR"
    case 4:
      return "MAY"
    case 5:
      return "JUN"
    case 6:
      return "JUL"
    case 7:
      return "AUG"
    case 8:
      return "SEP"
    case 9:
      return "OCT"
    case 10:
      return "NOV"
    case 11:
      return "DEC"
    default:
      return "UNKNOWN"
  }
}

export function getConvertedDate(dateString?: string) {
  if (dateString) {
    const date = new Date(dateString)
    const day = convertDay(date.getDay())
    const month = convertMonth(date.getMonth())
    return `${day} ${month} ${date.getDate()} ${date.getFullYear()}`
  }
  return "UNKNOWN"
}

/**
 * Compiles time in the correct format: HH:MM:SS.
 */
export function getTime(dateString?: string) {
  if (dateString) {
    const date = new Date(dateString)
    return date.toLocaleTimeString()
  }
  return
}

/**
 * Compiles direction URL for Google Maps using city and venue.
 */
export function getDirection(city?: string, venue?: string) {
  // venue.direction is all the same for all events, so it needs to be replaced with the correct data
  if (!city && !venue) {
    return
  }
  city = city?.replace(/\s/g, '+') // Replaces all spaces with +
  return `https://www.google.com/maps/dir//${venue}+${city}`
}
