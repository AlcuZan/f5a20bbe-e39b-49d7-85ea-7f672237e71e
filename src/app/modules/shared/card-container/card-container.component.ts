import {Component, HostListener, Input, OnInit} from '@angular/core';
import {MusicEvent} from "../../../model/events.model";
import {addToCart, removeFromCart} from "../../shopping-cart/store/shopping-cart.actions";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.reducer";
import {Router} from "@angular/router";
import {selectAllDates} from "../../home/store/home.selectors";
import {Observable} from "rxjs";
import {selectDatesOfEventsInShoppingCart} from "../../shopping-cart/store/shopping-cart.selectors";

@Component({
  selector: 'app-card-container',
  templateUrl: './card-container.component.html',
  styleUrls: ['./card-container.component.scss']
})
export class CardContainerComponent implements OnInit {
  @Input()
  eventData: MusicEvent[] = [];
  @Input()
  isInCart: boolean = false
  dates$: Observable<Set<string>> = new Observable<Set<string>>();
  headerSticky: boolean = false;

  constructor(private store: Store<AppState>, public router: Router) {
  }

  ngOnInit(): void {
    this.dates$ = this.isInCart ? this.store.select(selectDatesOfEventsInShoppingCart) : this.store.select(selectAllDates)

  }

  @HostListener('window:scroll', ['$event'])
  onScroll() {
    this.headerSticky = window.scrollY > 100;
  }

  getHeaderClass(){
    return this.headerSticky ? "sticky" : "not-sticky"
  }

  onClickAddToCart(musicEvent: MusicEvent) {
    this.store.dispatch(addToCart({musicEvent}))
  }

  onClickRemoveFromCart(musicEvent: MusicEvent){
    this.store.dispatch(removeFromCart({musicEvent}))
  }

  onLocationClicked(location?: string) {
    window.open(location, "_blank")
  }

  filterEventDataByDate(eventData: MusicEvent[], date: string) {
    return eventData.filter((ev) => ev.dateConverted === date)
  }
}
